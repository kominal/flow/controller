import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface TransactionsImportJob {
	_id?: string;
	tenantId: string;
	created: Date;
	type: string;
	accountId: string;
	from: Date;
	to: Date;
	started?: Date;
	completed?: Date;
	status?: string;
	successful?: number;
	failed?: number;
	reason?: string;
}

export const TransactionsImportJobDatabase = model<Document & TransactionsImportJob>(
	'TransactionsImportJob',
	new Schema(
		{
			tenantId: Types.ObjectId,
			created: Date,
			accountId: { type: Types.ObjectId, ref: 'Account' },
			type: String,
			from: Date,
			to: Date,
			started: Date,
			completed: Date,
			status: String,
			successful: Number,
			failed: Number,
			reason: String,
		},
		{ minimize: false }
	).index({ tenantId: -1 })
);
