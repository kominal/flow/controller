import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Invoice {
	_id?: string;
	tenantId: string;
	participantId: string;
	transactionIds: string[];
	estimateIds: string[];
	projectIds: string[];
}

export const InvoiceDatabase = model<Document & Invoice>(
	'Invoice',
	new Schema(
		{
			tenantId: Types.ObjectId,
			participantId: { type: Types.ObjectId, ref: 'Participant' },
			transactionIds: [{ type: Schema.Types.ObjectId, ref: 'Transaction' }],
			estimateIds: [{ type: Schema.Types.ObjectId, ref: 'Estimate' }],
			projectIds: [{ type: Schema.Types.ObjectId, ref: 'Project' }],
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);
