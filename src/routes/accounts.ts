import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { Account, AccountDatabase } from '../models/account';

export const accountsRouter = new Router();

accountsRouter.getAsUser<{ tenantId: string }, PaginationResponse<Account>, PaginationRequest>(
	'/:tenantId/accounts',
	async (req, res, userId) => {
		const responseBody = await applyPagination<Account>(AccountDatabase, req.query, {
			filter: { tenantId: Types.ObjectId(req.params.tenantId) },
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

accountsRouter.getAsUser<{ tenantId: string; accountId: string }, Account, never>(
	'/:tenantId/accounts/:accountId',
	async (req, res, userId) => {
		return {
			statusCode: 200,
			responseBody: await AccountDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.accountId })
				.lean()
				.orFail(new Error('transl.error.notFound.account')),
		};
	}
);

accountsRouter.postAsUser<{ tenantId: string }, Account & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/accounts',
	async (req, res, userId) => {
		delete req.body._id;

		const account = await AccountDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			created: new Date(),
		});

		return {
			statusCode: 200,
			responseBody: {
				_id: account._id,
			},
		};
	}
);

accountsRouter.putAsUser<{ tenantId: string; accountId: string }, Account & { tenantId: never }, never, never>(
	'/:tenantId/accounts/:accountId',
	async (req, res, userId) => {
		const update: any = Object.assign({}, req.body);
		update.tenantId = req.params.tenantId;
		delete update.created;

		await AccountDatabase.updateMany({ _id: req.params.accountId }, update);
		return {
			statusCode: 200,
		};
	}
);

accountsRouter.deleteAsUser<{ tenantId: string; accountId: string }, never, never>('/:tenantId/accounts/:accountId', async (req) => {
	await AccountDatabase.deleteMany({ _id: req.params.accountId, tenantId: req.params.tenantId });
	return {
		statusCode: 200,
	};
});
