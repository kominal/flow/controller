import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { Participant, ParticipantDatabase } from '../models/participant';

export const participantsRouter = new Router();

participantsRouter.getAsUser<{ tenantId: string }, PaginationResponse<Participant>, PaginationRequest>(
	'/:tenantId/participants',
	async (req) => {
		const responseBody = await applyPagination<Participant>(ParticipantDatabase, req.query, {
			filter: { tenantId: Types.ObjectId(req.params.tenantId) },
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

participantsRouter.getAsUser<{ tenantId: string; participantId: string }, Participant, never>(
	'/:tenantId/participants/:participantId',
	async (req) => {
		return {
			statusCode: 200,
			responseBody: await ParticipantDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.participantId }).orFail(
				new Error('transl.error.notFound.participant')
			),
		};
	}
);

participantsRouter.postAsUser<{ tenantId: string }, Participant & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/participants',
	async (req) => {
		delete req.body._id;

		const participant = await ParticipantDatabase.create({ ...req.body, tenantId: req.params.tenantId });

		return {
			statusCode: 200,
			responseBody: {
				_id: participant._id,
			},
		};
	}
);

participantsRouter.putAsUser<{ tenantId: string; participantId: string }, Participant & { tenantId: never }, never, never>(
	'/:tenantId/participants/:participantId',
	async (req, res, userId) => {
		await ParticipantDatabase.updateMany(
			{ _id: req.params.participantId },
			{ ...req.body, tenantId: req.params.tenantId, ownerId: userId }
		);
		return {
			statusCode: 200,
		};
	}
);

participantsRouter.deleteAsUser<{ tenantId: string; participantId: string }, never, never>(
	'/:tenantId/participants/:participantId',
	async (req) => {
		await ParticipantDatabase.deleteMany({ _id: req.params.participantId, tenantId: req.params.tenantId });
		return {
			statusCode: 200,
		};
	}
);
