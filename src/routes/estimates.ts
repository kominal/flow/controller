import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { Estimate, EstimateDatabase } from '../models/estimate';

export const estimatesRouter = new Router();

estimatesRouter.getAsUser<{ tenantId: string }, PaginationResponse<Estimate>, PaginationRequest>('/:tenantId/estimates', async (req) => {
	const responseBody = await applyPagination<Estimate>(EstimateDatabase, req.query, {
		filter: { tenantId: Types.ObjectId(req.params.tenantId) },
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

estimatesRouter.getAsUser<{ tenantId: string; estimateId: string }, Estimate, never>('/:tenantId/estimates/:estimateId', async (req) => {
	return {
		statusCode: 200,
		responseBody: await EstimateDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.estimateId }).orFail(
			new Error('transl.error.notFound.estimate')
		),
	};
});

estimatesRouter.postAsUser<{ tenantId: string }, Estimate & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/estimates',
	async (req) => {
		delete req.body._id;

		const estimate = await EstimateDatabase.create({ ...req.body, tenantId: req.params.tenantId });

		return {
			statusCode: 200,
			responseBody: {
				_id: estimate._id,
			},
		};
	}
);

estimatesRouter.putAsUser<{ tenantId: string; estimateId: string }, Estimate & { tenantId: never }, never, never>(
	'/:tenantId/estimates/:estimateId',
	async (req) => {
		await EstimateDatabase.updateMany({ _id: req.params.estimateId }, { ...req.body, tenantId: req.params.tenantId });
		return {
			statusCode: 200,
		};
	}
);

estimatesRouter.deleteAsUser<{ tenantId: string; estimateId: string }, never, never>('/:tenantId/estimates/:estimateId', async (req) => {
	await EstimateDatabase.deleteMany({ _id: req.params.estimateId, tenantId: req.params.tenantId });
	return {
		statusCode: 200,
	};
});
