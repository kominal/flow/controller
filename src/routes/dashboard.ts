import { Router } from '@kominal/lib-node-express-router';
import { PaginationRequest } from '@kominal/lib-node-mongodb-interface';
import { AccountDatabase } from '../models/account';
import { TransactionDatabase } from '../models/transaction';

export const dashboardRouter = new Router();

dashboardRouter.getAsUser<
	{ tenantId: string },
	{ transactions: number; revenue: { [key: string]: number }; expenses: { [key: string]: number } },
	PaginationRequest
>('/:tenantId/dashboard', async (req) => {
	const accounts = await AccountDatabase.find({ tenantId: req.params.tenantId });

	const date = new Date();
	const firstDayOfMonth = new Date(date.getFullYear(), date.getMonth(), 1);
	const lastDayOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);

	let revenue: { [key: string]: number } = {};
	let expenses: { [key: string]: number } = {};

	const transactions = await TransactionDatabase.find({
		tenantId: req.params.tenantId,
		time: { $gte: firstDayOfMonth, $lt: lastDayOfMonth },
	});

	for (const transaction of transactions) {
		if (transaction.amount !== 0) {
			const debtorAccount = accounts.find((a) => String(a._id) === String(transaction.debtorAccountId));
			const creditorAccount = accounts.find((a) => String(a._id) === String(transaction.creditorAccountId));
			let internalChange = 'NEUTRAL';
			if (transaction.amount !== 0) {
				if (debtorAccount?.internal && !creditorAccount?.internal) {
					internalChange = transaction.amount > 0 ? 'NEGATIVE' : 'POSITIVE';
				} else if (!debtorAccount?.internal && creditorAccount?.internal) {
					internalChange = transaction.amount > 0 ? 'POSITIVE' : 'NEGATIVE';
				}
			}

			if (internalChange === 'POSITIVE') {
				revenue[transaction.currency] = (revenue[transaction.currency] || 0) + transaction.amount;
			} else if (internalChange === 'NEGATIVE') {
				expenses[transaction.currency] = (expenses[transaction.currency] || 0) + transaction.amount;
			}
		}
	}

	return {
		statusCode: 200,
		responseBody: {
			transactions: transactions.length,
			revenue,
			expenses,
		},
	};
});
