import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { createReadStream, readdirSync, readFileSync } from 'fs';
import moment from 'moment';
import { dirSync } from 'tmp';
import unzipper from 'unzipper';
import { XmlDocument } from 'xmldoc';
import { Transaction } from '../models/transaction';
import { TransactionsImportJob, TransactionsImportJobDatabase } from '../models/transactionsImportJob';

export const transactionsImportJobsRouter = new Router();

transactionsImportJobsRouter.getAsUser<{ tenantId: string }, never, PaginationResponse<TransactionsImportJob>, PaginationRequest>(
	'/:tenantId/transactionsImportJobs',
	async (req) => {
		const responseBody = await applyPagination<TransactionsImportJob>(TransactionsImportJobDatabase, req.query, {
			filter: { tenantId: Types.ObjectId(req.params.tenantId) },
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

transactionsImportJobsRouter.getAsUser<{ tenantId: string; transactionsImportJobId: string }, never, TransactionsImportJob, never>(
	'/:tenantId/transactionsImportJobs/:transactionsImportJobId',
	async (req) => {
		return {
			statusCode: 200,
			responseBody: await TransactionsImportJobDatabase.findOne({
				tenantId: req.params.tenantId,
				_id: req.params.transactionsImportJobId,
			}).orFail(new Error('transl.error.notFound.transactionsImportJob')),
		};
	}
);

transactionsImportJobsRouter.postAsUser<{ tenantId: string }, TransactionsImportJob & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/transactionsImportJobs',
	async (req) => {
		console.log(req.files);

		delete req.body._id;
		delete req.body.started;
		delete req.body.completed;
		delete req.body.successful;
		delete req.body.failed;

		console.log('Upload started...');
		const path = dirSync().name;
		await new Promise((resolve) => createReadStream('<Path>').pipe(unzipper.Extract({ path })).on('close', resolve));

		let count = 0;

		for (const fileName of readdirSync(path)) {
			const xml = new XmlDocument(readFileSync(path + '/' + fileName).toString('utf-8'));
			const transactions = xml.childNamed('BkToCstmrAcctRpt')?.childNamed('Rpt')?.childrenNamed('Ntry') || [];

			for (const transaction of transactions) {
				const amountChild = transaction.childNamed('Amt');
				const timeChild = transaction.childNamed('ValDt')?.childNamed('Dt');
				const additionalInformationChild = transaction.childNamed('AddtlNtryInf');
				const transactionDetailsChild = transaction.childNamed('NtryDtls')?.childNamed('TxDtls');
				const purposeChild = transactionDetailsChild?.childNamed('RmtInf')?.childNamed('Ustrd');
				const endToEndIdChild = transactionDetailsChild?.childNamed('Refs');
				const relatedPartiesChild = transactionDetailsChild?.childNamed('RltdPties');
				const debtorAccountNameChild = relatedPartiesChild?.childNamed('Dbtr')?.childNamed('Nm');
				const debtorAccountIdChild = relatedPartiesChild?.childNamed('DbtrAcct')?.childNamed('Id')?.childNamed('IBAN');
				const debtorAccountOtherIdChild = relatedPartiesChild
					?.childNamed('DbtrAcct')
					?.childNamed('Id')
					?.childNamed('Othr')
					?.childNamed('Id');
				const creditorAccountNameChild = relatedPartiesChild?.childNamed('Cdtr')?.childNamed('Nm');
				const creditorAccountIdChild = relatedPartiesChild?.childNamed('CdtrAcct')?.childNamed('Id')?.childNamed('IBAN');
				const creditorAccountOtherIdChild = relatedPartiesChild
					?.childNamed('CdtrAcct')
					?.childNamed('Id')
					?.childNamed('Othr')
					?.childNamed('Id');

				if (!amountChild || !timeChild || !transactionDetailsChild) {
					continue;
				}

				const endToEndId = endToEndIdChild?.val;

				if (endToEndId != 'NOTPROVIDED') {
					//Check if ID already in DB
				}

				const flowTransaction: Transaction = {
					tenantId: '',
					debtorAccountId: '',
					creditorAccountId: '',
					amount: Number(amountChild.val),
					currency: amountChild.attr.Ccy,
					purpose: purposeChild?.val,
					time: moment(timeChild.val, 'YYYY-MM-DD').toDate(),
					type: '',
				};
				count++;

				console.log(flowTransaction);
			}
		}

		const transactionsImportJob = await TransactionsImportJobDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			created: new Date(),
			type: 'MANUAL',
			status: 'PLANNED',
		});

		return {
			statusCode: 200,
			responseBody: {
				_id: transactionsImportJob._id,
			},
		};
	}
);

transactionsImportJobsRouter.deleteAsUser<{ tenantId: string; transactionsImportJobId: string }, never, never, never>(
	'/:tenantId/transactionsImportJobs/:transactionsImportJobId',
	async (req) => {
		await TransactionsImportJobDatabase.updateMany(
			{ _id: req.params.transactionsImportJobId, tenantId: req.params.tenantId },
			{ status: 'CANCELLED' }
		);
		return {
			statusCode: 200,
		};
	}
);
