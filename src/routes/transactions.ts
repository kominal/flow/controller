import { Router } from '@kominal/lib-node-express-router';
import { applyPaginationWithMapping, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { Account, AccountDatabase } from '../models/account';
import { Transaction, TransactionDatabase } from '../models/transaction';

export const transactionsRouter = new Router();

transactionsRouter.getAsUser<
	{ tenantId: string },
	PaginationResponse<
		Transaction & { debtorAccount: Account; creditorAccount: Account; internalChange: 'POSITIVE' | 'NEGATIVE' | 'NEUTRAL' }
	>,
	PaginationRequest
>('/:tenantId/transactions', async (req) => {
	const responseBody = await applyPaginationWithMapping<Transaction>(
		TransactionDatabase,
		req.query,
		async (transaction) => {
			const debtorAccount = await AccountDatabase.findById(transaction.debtorAccountId);
			const creditorAccount = await AccountDatabase.findById(transaction.creditorAccountId);

			let internalChange = 'NEUTRAL';
			if (transaction.amount !== 0) {
				if (debtorAccount?.internal && !creditorAccount?.internal) {
					internalChange = transaction.amount > 0 ? 'NEGATIVE' : 'POSITIVE';
				} else if (!debtorAccount?.internal && creditorAccount?.internal) {
					internalChange = transaction.amount > 0 ? 'POSITIVE' : 'NEGATIVE';
				}
			}

			return {
				...transaction.toJSON(),
				debtorAccount,
				creditorAccount,
				internalChange,
			};
		},
		{
			filter: { tenantId: Types.ObjectId(req.params.tenantId) },
		}
	);

	return {
		statusCode: 200,
		responseBody,
	};
});

transactionsRouter.getAsUser<{ tenantId: string; transactionId: string }, Transaction, never>(
	'/:tenantId/transactions/:transactionId',
	async (req) => {
		return {
			statusCode: 200,
			responseBody: await TransactionDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.transactionId }).orFail(
				new Error('transl.error.notFound.transaction')
			),
		};
	}
);

transactionsRouter.postAsUser<{ tenantId: string }, Transaction & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/transactions',
	async (req) => {
		delete req.body._id;

		const transaction = await TransactionDatabase.create({ ...req.body, tenantId: req.params.tenantId });

		return {
			statusCode: 200,
			responseBody: {
				_id: transaction._id,
			},
		};
	}
);

transactionsRouter.putAsUser<{ tenantId: string; transactionId: string }, Transaction & { tenantId: never }, never, never>(
	'/:tenantId/transactions/:transactionId',
	async (req, res, userId) => {
		await TransactionDatabase.updateMany(
			{ _id: req.params.transactionId },
			{ ...req.body, tenantId: req.params.tenantId, ownerId: userId }
		);
		return {
			statusCode: 200,
		};
	}
);

transactionsRouter.deleteAsUser<{ tenantId: string; transactionId: string }, never, never>(
	'/:tenantId/transactions/:transactionId',
	async (req) => {
		await TransactionDatabase.deleteMany({ _id: req.params.transactionId, tenantId: req.params.tenantId });
		return {
			statusCode: 200,
		};
	}
);
