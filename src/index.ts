import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { error, info, startObserver } from '@kominal/observer-node-client';
import { exit } from 'process';
import { accountsRouter } from './routes/accounts';
import { categoriesRouter } from './routes/categories';
import { dashboardRouter } from './routes/dashboard';
import { estimatesRouter } from './routes/estimates';
import { invoicesRouter } from './routes/invoices';
import { participantsRouter } from './routes/participants';
import { projectsRouter } from './routes/projects';
import { tenantsRouter } from './routes/tenants';
import { transactionsRouter } from './routes/transactions';
import { transactionsImportJobsRouter } from './routes/transactionsImportJobs';
import { PaymentsImportJobCreationScheduler } from './scheduler/paymentsImportJobCreation.scheduler';
import { PaymentsImportJobExecutionScheduler } from './scheduler/paymentsImportJobExecution.scheduler';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;

async function start() {
	try {
		startObserver();
		mongoDBInterface = new MongoDBInterface('controller');
		await mongoDBInterface.connect();
		expressRouter = new ExpressRouter({
			baseUrl: 'controller',
			healthCheck: async () => true,
			routes: [
				accountsRouter,
				categoriesRouter,
				estimatesRouter,
				invoicesRouter,
				participantsRouter,
				transactionsRouter,
				projectsRouter,
				tenantsRouter,
				dashboardRouter,
				transactionsImportJobsRouter,
			],
		});
		await expressRouter.start();
		await new PaymentsImportJobCreationScheduler().start(3600);
		await new PaymentsImportJobExecutionScheduler().start(60);
	} catch (e) {
		error(`Failed to start service: ${e}`);
		exit(1);
	}
}
start();

process.on('SIGTERM', async () => {
	info(`Received system signal 'SIGTERM'. Shutting down service...`);
	expressRouter.getServer()?.close();
	await mongoDBInterface.disconnect();
});
